$(document).ready(function () {

    function AnasayfaIlanlar() {
        $.ajax({
            url: "assets/ajax/getLatestItems.php", type: "POST", data: {limit: 17, counter: 0}, success: function (data, textStatus, jqXHR) {
                $('.latestItemsRow').html(data);
            }, error: function (jqXHR, textStatus, errorThrown) {
            }
        });
        $('.latestItemsRowButton').click(function () {
            var getLastItemCounter = $('.latestItemInner:last').data('counter');
            $.ajax({
                url: "assets/ajax/getLatestItems.php", type: "POST", data: {limit: 17, counter: (getLastItemCounter + 1)}, success: function (data, textStatus, jqXHR) {
                    $('.latestItemsRow').append(data);
                }
            });
        });
    }

    function KategoriIlanlar() {
        var getCategoryID = $('.categoryID').data('id');
        $.ajax({
            url: "assets/ajax/getLatestItemsCategory.php", type: "POST", data: {limit: 17, counter: 0, category: getCategoryID}, success: function (data, textStatus, jqXHR) {
                $('.latestItemsRowCategory').html(data);
            }, error: function (jqXHR, textStatus, errorThrown) {
            }
        });
        $('.latestItemsRowButtonCategory').click(function () {
            var getLastItemCounter = $('.latestItemInner:last').data('counter');
            $.ajax({
                url: "assets/ajax/getLatestItemsCategory.php",
                type: "POST",
                data: {limit: 8, counter: (getLastItemCounter + 1), category: getCategoryID},
                success: function (data, textStatus, jqXHR) {
                    $('.latestItemsRowCategory').append(data);
                }
            });
        });
    }


    AnasayfaIlanlar();
    KategoriIlanlar();
    $('#il').change(function () {
        var getIlID = $('#il option:selected').attr('data-id');

        $.ajax({
            url: "js/ajax/getCountryForCity.php", type: "POST", data: {city_id: getIlID}, success: function (data, textStatus, jqXHR) {

                $('#getIlceForIl').html(data);
            }
        });
    });

    $('#getIlceForIl').change(function () {
        var getIlID = $('#getIlceForIl option:selected').attr('data-id');

        $.ajax({
            url: "js/ajax/getCountryForMah.php", type: "POST", data: {city_id: getIlID}, success: function (data, textStatus, jqXHR) {

                $('#getMahalle').html(data);
            }
        });
    });

    admin.plugins.init();
    admin.plugins.Gmap.init();

});


var admin = {
    plugins: {
        init: function () {
            if ($('*[data-update]').is("select")) {
                $('*[data-update]').change(function () {
                    if ($(this).data('update') == 'update_map') {
                        admin.plugins.Gmap.update_map($(this), $(this).val());
                    }
                });
            } else if ($('*[data-update]').is("input")) {
                $('*[data-update]').click(function () {
                    admin.plugins.Gmap.update_map($(this), $(this).attr('value'));
                });
            }
        },

        Gmap: {
            map: null,
            geocoder: null,
            markers: [],
            Lat: null,
            Lng: null,
            init: function () {
                if ($('#Gmap').length > 0) {
                    admin.plugins.Gmap.geocoder = new google.maps.Geocoder();
                    var mapOptions = {
                        zoom: 5,
                        center: new google.maps.LatLng(39, 34)
                    };
                    admin.plugins.Gmap.map = new google.maps.Map(document.getElementById('Gmap'), mapOptions);
                }
            },
            update_map: function (element, value) {

                if ($(element).is("select")) {
                    var text = $(element).find('option:selected').text();
                } else {
                    var text = $(element).val();
                }
                admin.plugins.Gmap.deleteMarkers();
                admin.plugins.Gmap.geocoder.geocode({'address': text}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        admin.plugins.Gmap.Lat = results[0].geometry.location.lat();
                        admin.plugins.Gmap.Lng = results[0].geometry.location.lng();

                        if ((results && results[0] && results[0].formatted_address) && (results[0].formatted_address == "Antarctica")) {
                            admin.plugins.Gmap.SetLat = -75;
                            admin.plugins.Gmap.SetLng = 0;
                            admin.plugins.Gmap.SetZoom = 2;
                            admin.plugins.Gmap.map.setCenter(new google.maps.LatLng(admin.plugins.Gmap.Lat, admin.plugins.Gmap.Lng));
                            admin.plugins.Gmap.map.setZoom(admin.plugins.Gmap.SetZoom);
                        }
                        else if (results && results[0] && results[0].geometry && results[0].geometry.viewport) {
                            admin.plugins.Gmap.map.fitBounds(results[0].geometry.viewport);
                        }
                        else if (results && results[0] && results[0].geometry && results[0].geometry.bounds) {
                            admin.plugins.Gmap.map.fitBounds(results[0].geometry.bounds);
                        }

                        admin.plugins.Gmap.map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: admin.plugins.Gmap.map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: results[0].geometry.location
                        });
                        google.maps.event.addDomListener(marker, 'dragend', function (event) {
                            admin.plugins.Gmap.Lat = event.latLng.lat();
                            admin.plugins.Gmap.Lng = event.latLng.lng();
                            admin.plugins.Gmap.update_input();
                        });
                        admin.plugins.Gmap.markers.push(marker);
                        admin.plugins.Gmap.update_input();
                    }
                });
            },
            update_input: function () {
                if ($('#Lat').length > 0) {
                    $('#Lat').val(admin.plugins.Gmap.Lat);
                }
                if ($('#Lng').length > 0) {
                    $('#Lng').val(admin.plugins.Gmap.Lng);
                }
            },
            deleteMarkers: function () {
                for (var i = 0; i < admin.plugins.Gmap.markers.length; i++) {
                    admin.plugins.Gmap.markers[i].setMap(null);
                }
                admin.plugins.Gmap.markers = [];
            }
        },
    }
};
